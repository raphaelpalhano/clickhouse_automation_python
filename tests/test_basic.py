import testflows.core as tf
from context import helpers as hp

with tf.Feature("Connections"):

  with tf.Scenario("basic validations clickhouse"):
    with tf.Given("I have something"):
      valid_output = 1
    with tf.When("I do some"):
        output = hp.ch_select("1")
    with tf.Then("I expect something"):
        assert output == valid_output
    
