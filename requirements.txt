certifi==2023.7.22
charset-normalizer==3.2.0
clickhouse-connect==0.6.9
## !! Could not determine repository location
-e c:\users\rapha\projects\click_house_automation_py
idna==3.4
lz4==4.3.2
pytz==2023.3
requests==2.31.0
testflows==1.9.71
testflows.asserts==6.5.230626.1133416
testflows.connect==1.7.230414.1210340
testflows.core==1.9.230705.1122619
testflows.database==1.6.200713.1142213
testflows.stash==1.1.230317.1211113
testflows.uexpect==1.7.230414.1210501
urllib3==2.0.4
zstandard==0.21.0
