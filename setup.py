from distutils.core import setup
from setuptools import find_namespace_packages


URL = 'https://gitlab.com/raphaelpalhano/clickhouse_automation_python'
VERSION = '1.0.0'



setup(
    name='clickhouseAutomation',
    version=VERSION,
    description='Clickhouse automation with Python',
    author='Raphael Palhano',
    url=URL,
    packages=find_namespace_packages(),
    license='MIT',
    keywords='ClickHouse db database cloud analytics',
    long_description_content_type="text/x-rst",
    python_requires='>=3.7, <4',
    install_requires=[
        "testflows.core",
        "testflows",
        "clickhouse_connect",
        "testflows.asserts",
        "testflows.connect",
        "testflows.database",
        "testflows.stash",
    ],

)