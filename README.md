# Clickhouse automation project


## Setup config

pip install -e .

base: `https://docs.python.org/3/distutils/setupscript.html`

## Structure

base `https://docs.python-guide.org/writing/structure/#structuring-your-project`
`https://kennethreitz.org/essays/2013/01/27/repository-structure-and-python` 


## Docker

### Using docker cli

Start server:

~~~bash
docker run -d --name some-clickhouse-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server
~~~

Connect native client:

~~~bash
docker run -it --rm --link some-clickhouse-server:clickhouse-server --entrypoint clickhouse-client clickhouse/clickhouse-server --host clickhouse-server
# OR
docker exec -it some-clickhouse-server clickhouse-client
~~~

~~~bash

docker run -d \
    -v $(realpath ./ch_data):/var/lib/clickhouse/ \
    -v $(realpath ./ch_logs):/var/log/clickhouse-server/ \
    --name some-clickhouse-server --ulimit nofile=262144:262144 clickhouse/clickhouse-server

~~~

Stopp or remove container:

~~~bash

docker stop some-clickhouse-server
docker rm some-clickhouse-server

~~~


### Docker Compose

Create service server connection:

~~~yaml

ch_server:
    image: yandex/clickhouse-server
    ports:
      - "8123:8123"
    volumes:
      - ./db:/var/lib/clickhouse
    networks:
        - ch_ntw
~~~

Create a client connection:

~~~yaml
ch_client:
    image: yandex/clickhouse-client
    entrypoint:
      - /bin/sleep
    command:
      - infinity
    networks:
        - ch_ntw
~~~

Networking:


~~~yaml

networks:
  ch_ntw:
    driver: bridge
    ipam:
      config:
        - subnet: 10.222.1.0/24
~~~
