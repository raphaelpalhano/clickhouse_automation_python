from .core import connect_ch


client = connect_ch()

def ch_select(query):
  output = client.command(f"SELECT {query}")
  return output

def ch_insert(query):
  output = client.command(f"INSERT INTO {query}")
  return output

def ch_delete(query):
  output = client.command(f"DELETE FROM {query}")
  return output

